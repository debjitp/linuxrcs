# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH
export PATH=/home/debjit/Tools/proto_compiler/bin:$PATH
export PKG_CONFIG_PATH=/home/debjit/Tools/condaenv/gem5/lib/pkgconfig:$PKG_CONFIG_PATH
# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
export PATH=/opt/cisco/anyconnect/bin:$PATH
alias zhang-21="ssh zhang-21.ece.cornell.edu -l dp638 -X"
alias zhang-22="ssh zhang-22.ece.cornell.edu -l dp638 -X"
alias zhang-01="ssh zhang-01.ece.cornell.edu -l dp638 -X"

alias agem5="conda activate /home/debjit/Tools/condaenv/gem5"
alias cde="conda deactivate"

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/debjit/Tools/anaconda2/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/debjit/Tools/anaconda2/etc/profile.d/conda.sh" ]; then
        . "/home/debjit/Tools/anaconda2/etc/profile.d/conda.sh"
    else
        export PATH="/home/debjit/Tools/anaconda2/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BSAH_SELECT=1
. /usr/lib/python2.7/site-packages/powerline_status-2.7.dev9999+git.b0ea99430c00713279f7e3c37aa6c63a85b68ad5-py2.7.egg/powerline/bindings/bash/powerline.sh
