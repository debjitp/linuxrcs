se nu
syn on
set smartindent
set autoindent
set background=light
set ruler
"set laststatus=2
colorscheme sol-term
syntax on
filetype indent plugin on
set tabstop=4 expandtab shiftwidth=4 softtabstop=4
"set guifont=Consolas:h11

" Setting the backspace problem for Windows
" Without this my backspace key was not working in Windows
set bs=2 
set bs=indent,eol,start
" Setting up file specific colorscheme
autocmd FileType c colorscheme ChocolateLiquor
autocmd FileType cpp colorscheme ChocolateLiquor
autocmd FileType verilog colorscheme MountainDew
autocmd FileType latex colorscheme mac_classic 
autocmd FileType lex colorscheme random 
autocmd FileType yacc colorscheme kate
autocmd FileType python colorscheme Chasing_Logic
autocmd FileType systemverilog colorscheme paintbox

if has('gui_running')
  set guioptions-=T  " no toolbar
  set lines=50 columns=100 linespace=0
  if has('gui_win32')
    set guifont=DejaVu_Sans_Mono:h10:cANSI
  else
    set guifont=DejaVu\ Sans\ Mono\ 11
  endif
endif

"LaTeX suite specific settings
set shellslash
set grepprg=grep\ -nH\ $*
filetype indent on
let g:tex_flavor='latex'

"Opening in a new tab
nnoremap tf <C-W>gf
vnoremap tf <C-W>gf

"Opening in a new window
nnoremap nf <C-W>f
vnoremap nf <C-W>f

set t_Co=256

"highlight DiffAdd cterm=none ctermfg=Cyan ctermbg=Blue guifg=Magenta guibg=Blue
"highlight DiffDelete cterm=none ctermfg=Cyan ctermbg=Blue guifg=Magenta guibg=Blue
"highlight DiffChange cterm=none ctermfg=Cyan ctermbg=Blue guifg=Magenta guibg=Blue
"highlight DiffText cterm=none ctermfg=Cyan ctermbg=White guifg=Magenta guibg=White

"autocmd bufnewfile *.py so /work/zhang-x1/users/dp638/Tools/vim/templates/python_header.txt
"autocmd bufnewfile *.py exe "1," . 20 . "g/FILE:.*/s//FILE: " .expand("%")
"autocmd bufnewfile *.py exe "1," . 20 . "g/CREATED:.*/s//CREATED: " .strftime("%d-%m-%Y")
"autocmd Bufwritepre,filewritepre *.py execute "normal ma"
"autocmd Bufwritepre,filewritepre *.py exe "1," . 20 . "g/LMODIFIED:.*/s/LMODIFIED:.*/LMODIFIED: " .strftime("%c")
"autocmd bufwritepost,filewritepost *.py execute "normal `a"

highlight DiffAdd    cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
highlight DiffDelete cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
highlight DiffChange cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
highlight DiffText   cterm=bold ctermfg=10 ctermbg=88 gui=none guifg=bg guibg=Red

set incsearch
set hlsearch
hi Search cterm=NONE ctermfg=White ctermbg=Brown

hi CursorLine   cterm=NONE ctermbg=darkred ctermfg=white guibg=darkred guifg=white
hi CursorColumn cterm=NONE ctermbg=darkred ctermfg=white guibg=darkred guifg=white
nnoremap <Leader>c :set cursorline! cursorcolumn!<CR>
